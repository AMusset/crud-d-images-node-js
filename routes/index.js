var express = require('express');
var router = express.Router();
const MongoClient = require('mongodb').MongoClient;
const assert = require('assert');
const dburl = 'mongodb://localhost:27017';
const ObjectId = require('mongodb').ObjectId;

/* GET home page. 
router.get('/', function(req, res, next) {
	const collection = db.collection('people');
  	var datap = collection.find();
  	res.render('index', { 
  		title: 'Wrestling World',
  		personnes: datap 
  	});
});
*/

/*
app.get('/admin', routes.article.admin)
exports.admin = (req, res, next) => {
  req.collections.articles.find({}, {sort: {_id: -1}}).toArray((error, articles) => {
    if (error) return next(error)
    res.render('admin', {articles: articles})
  })
}
*/

router.get('/', function(req, res) {

  // connect to DB
  MongoClient.connect(dburl, function(err, client) {
    if (!err) {

      	// Get db
      	const db = client.db('photos_project');

      	// Get collection
      	const collection = db.collection('people');

      	// Find all documents in the collection
      	collection.find({}, {sort: {_id: -1}}).toArray(function(err, datap) {
        	if (!err) {

				// send output back
          		res.render('index', { 
  					title: 'Wrestling World',
  					personnes: datap
  		  		});
        	}
      	});

      // close db client
      client.close();
    }
  });
});

// Ajout
router.post('/addwrestler', function(req, res) {

	MongoClient.connect(dburl, function(err, client) {
    	if (!err) {

		    // Set our internal DB variable
		    const db = client.db('photos_project');

		    // Get our form values. These rely on the "name" attributes
		    var wName = req.body.newName;
		    var wPicture = req.body.newPicture;

		    // Set our collection
		    const collection = db.collection('people');

		    // Submit to the DB
		    collection.insertOne({
		        "name" : wName,
		        "picture" : wPicture
		    }, function (err, doc) {
		        if (err) {
		            // If it failed, return error
		            res.send("There was a problem adding the information to the database.");
		        }
		        else {
		            // And forward to success page
		            res.redirect("/");
		        }
		    });
		}
    });
});

// Suppression
router.post('/supprwrestler', function(req, res) {

	MongoClient.connect(dburl, function(err, client) {
    	if (!err) {

		    // Set our internal DB variable
		    const db = client.db('photos_project');

		    // Set our collection
		    const collection = db.collection('people');

		    // Submit to the DB
		    collection.deleteOne({
		        "_id" : ObjectId(req.body.toDeleteId)
		    }, function (err, doc) {
		        if (err) {
		            // If it failed, return error
		            res.send("There was a problem adding the information to the database.");
		        }
		        else {
		            // And forward to success page
		            res.redirect("/");
		        }
		    });
		}
    });
});

module.exports = router;
