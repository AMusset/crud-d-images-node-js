var express = require('express');
var router = express.Router();

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.render('modify', { 
  	title: 'Modifying in Wrestling World'
  });
});

module.exports = router;
