const mongo = require('mongodb')
const dbHost = '127.0.0.1'
const dbPort = '27017'
const {Db,Server} = mongo
const db = new Db('photos_project',new Server(dbHost,dbPort),{safe: true})
const collection = db.collection('people')

db.open((error,dbConnection)=>{
	console.log(db._state)

	collection.find().toArray((err, items) => {
  		console.log(items)
	})

	db.close()
})

